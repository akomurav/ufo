#include "ufo/InitializePasses.h"

#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Instructions.h"
#include "llvm/Function.h"
#include "llvm/Module.h"
#include "llvm/Support/Compiler.h"
#include "llvm/PassManager.h"

#include "llvm/Support/IRBuilder.h"

#include "boost/format.hpp"

#include <map>

using namespace llvm;

namespace 
{
  class NondetInit : public ModulePass 
  {

  private:
    /** map for nondet functions */
    std::map<const Type*,Constant*> ndfn;
    Module* m;
    
    Constant* getNondetFn (const Type *type)
    {
      Constant* res = ndfn [type];
      if (res == NULL)
	{
	  res = m->getOrInsertFunction 
	    (boost::str 
	     (boost::format ("nondet_%d") % ndfn.size ()), type, NULL);

	  if (Function *f = dyn_cast<Function>(res))
	    f->setDoesNotAccessMemory (true);
	  
	  ndfn[type] = res;
	}
      return res;
    }

  public:
    static char ID;
    NondetInit() : ModulePass(ID), m(NULL) {}    

    
    virtual bool runOnModule(Module &M) 
    {
      
      m = &M;
      bool Changed = false;

      //Iterate over all functions, basic blocks and instructions.
      for (Module::iterator FI = M.begin(), E = M.end(); FI != E; ++FI)
	{
	  for (Function::iterator b = FI->begin(), be = FI->end(); b != be; ++b)
	    {
	      for (BasicBlock::iterator i = b->begin(), ie = b->end(); 
		   i != ie;) 
		{
		  if (AllocaInst *AI = dyn_cast<AllocaInst>(i))
		    {
		      Constant *fun = getNondetFn (AI->getAllocatedType ());
		      
		      IRBuilder<> Builder(FI->getContext ());
		      Builder.SetInsertPoint (&*(++i));
		      Builder.CreateStore (Builder.CreateCall (fun), AI);
		      Changed = true;
		    }	
		  else
		    ++i;
		}
	    }
	}

      ndfn.clear ();
      m = NULL;
      
      return Changed;
    }

    virtual void getAnalysisUsage (AnalysisUsage &AU) const
    {
      AU.setPreservesCFG ();
    }
    
  };
char NondetInit::ID = 0;
}


//static RegisterPass<NondetInit> X("nondet-init", 
//				      "Store nondet() to every alloca address");
INITIALIZE_PASS(NondetInit, "nondet-init", 
		"Store nondet() to every alloca address", false, false)

