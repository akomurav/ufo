#ifndef __EXIT_RETURN__H_
#define __EXIT_RETURN__H_
#include "ufo/InitializePasses.h"

// This pass replaces 'exit(x)' in main with 'return x'. It is based
// on SimplifyLibCalls of LLVM 2.6

#include "llvm/Pass.h"
#include "llvm/Function.h"
#include "llvm/Instructions.h"
#include "llvm/Support/IRBuilder.h"

namespace llvm
{
  struct ExitReturn : public FunctionPass
  {
  public:
    static char ID;
    ExitReturn () : FunctionPass (ID) {}
    
    virtual void getAnalysisUsage (AnalysisUsage& AU) const;
    virtual bool runOnFunction (Function &F);
  private:
    Value* CallOptimizer (Function *Callee, CallInst *CI, IRBuilder<> &B);
    
  };
}


#endif
