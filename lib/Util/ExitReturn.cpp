#include "ExitReturn.hpp"

#include "llvm/BasicBlock.h"
#include "llvm/Function.h"
#include "llvm/Instructions.h"
#include "llvm/LLVMContext.h"

#include "llvm/ADT/STLExtras.h"

using namespace llvm;

char ExitReturn::ID = 0;

void ExitReturn::getAnalysisUsage (AnalysisUsage &AU) const
{
  AU.addPreserved ("mem2reg");
}

bool ExitReturn::runOnFunction (Function &F)
{
  // -- only works on main function
  if (F.getName ().compare ("main") != 0) return false;

  IRBuilder<> Builder(F.getContext());

  bool Changed = false;
  
  for (Function::iterator BB = F.begin(), E = F.end(); BB != E; ++BB) 
    {
      for (BasicBlock::iterator I = BB->begin(), E = BB->end(); I != E; ) 
	{
	  // Ignore non-calls.
	  CallInst *CI = dyn_cast<CallInst>(I++);
	  if (!CI) continue;

	  // Ignore indirect calls and calls to non-external functions.
	  Function *Callee = CI->getCalledFunction();
	  if (Callee == 0 || !Callee->isDeclaration() ||
	      !(Callee->hasExternalLinkage() || 
		Callee->hasDLLImportLinkage()))
	    continue;

	  // -- only handle exit function
	  if (Callee->getName ().compare ("exit") != 0) continue;
	  Builder.SetInsertPoint (BB, I);
	  
	  Value* Result = CallOptimizer (CI->getCalledFunction (), CI, Builder);
	  if (Result == 0) continue;
	  
	  Changed = true;
	  
	  // Inspect the instruction after the call (which was potentially just
	  // added) next.
	  I = CI; ++I;
      
	  if (CI != Result && !CI->use_empty()) {
	    CI->replaceAllUsesWith(Result);
	    if (!Result->hasName())
	      Result->takeName(CI);
	  }
	  CI->eraseFromParent();
	  
	  
	}
    }
  return Changed;
}

Value* ExitReturn::CallOptimizer (Function *Callee, CallInst *CI, 
				  IRBuilder<> &B)
{
  // We never change the calling convention.
  if (CI->getCallingConv() != llvm::CallingConv::C)
    return NULL;
  
  // Verify we have a reasonable prototype for exit.
  if (Callee->arg_size() == 0 || !CI->use_empty())
    return 0;

  Function *Caller = CI->getParent ()->getParent ();
  
  // Verify the caller is main, and that the result type of main matches the
  // argument type of exit.
  if (Caller->getName() != "main" || !Caller->hasExternalLinkage() ||
      Caller->getReturnType() != CI->getArgOperand(0)->getType())
    return 0;


  LLVMContext* Context = &CI->getCalledFunction ()->getContext ();
  TerminatorInst *OldTI = CI->getParent()->getTerminator();

  // Drop all successor phi node entries.
  for (unsigned i = 0, e = OldTI->getNumSuccessors(); i != e; ++i)
    OldTI->getSuccessor(i)->removePredecessor(CI->getParent());

  // Remove all instructions after the exit.
  BasicBlock::iterator Dead = CI, E = OldTI; ++Dead;
  while (Dead != E) {
    BasicBlock::iterator Next = next(Dead);
    if (Dead->getType() != Type::getVoidTy(*Context))
      Dead->replaceAllUsesWith(UndefValue::get(Dead->getType()));
    Dead->eraseFromParent();
    Dead = Next;
  }

  // Insert a return instruction.
  OldTI->eraseFromParent();
  B.SetInsertPoint(B.GetInsertBlock());
  B.CreateRet(CI->getArgOperand(0));

  return CI;
}


//static RegisterPass<ExitReturn> 
//X("exit-return", "Simplify calls to exit() in main() into returns");
INITIALIZE_PASS(ExitReturn, "exit-return", 
		"Simplify calls to exit() in main() into returns", false, false)


