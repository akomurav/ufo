#ifndef _UFO_INVARIANT__HPP_
#define _UFO_INVARIANT__HPP_

#include "ExprZ3.hpp"
#include "ufo/ufo.hpp"
#include "ufo/Arg.hpp"
#include "ufo/Expr.hpp"
#include "ufo/Asd/AbstractStateDomain.hpp"

namespace ufo{
  class Invariant{
    private:
    std::map<CutPointPtr, ExprZ3*> cp2solver;

    std::map<CutPointPtr, std::vector<Node*> > cp2nodes;
    ExprFactory& efac;

    AbstractStateDomain &adom;
    
    void simplifyCutPoint2(CutPointPtr cp){
      std::vector<Node*>& nodes = cp2nodes[cp];
      cp2inv[cp] = mk<FALSE>(efac);
      ExprZ3 z3(efac);
      z3.elim_and(true);
      z3.restart();

      
      std::vector<Expr> assumptions;
      std::set<Expr> trueAssumptions;
      
      std::map<Node*, Expr> node2lbl;

      foreach (Node* n, nodes){
        Expr lbl;
	if (n->getLabel())
	  node2lbl[n] = n->getLabel();
	else 
	  node2lbl[n] = adom.gamma(n->getLoc(), n->getState());
      }

      Expr formP = mk<FALSE>(efac);
      Expr formN = mk<TRUE>(efac);
      
      foreach (Node* n, nodes){
        formP = boolop::lor(formP, node2lbl[n]);
        //Expr assum = mk<ASM>(mkTerm(n,efac));
        //assumptions.push_back(assum);
        //formN = boolop::land(formN, mk<IMPL>(assum,mk<NEG>(node2lbl[n])));
        addAssumptions addVisit;
	Expr nodeLabel = mk<NEG>(node2lbl[n]);
	//Expr nodeLabel = dagVisit(addVisit, boolop::nnf (mk<NEG>(node2lbl[n])));
	Expr nlbl = mk<IMPL>(mk<ASM>(mkTerm(n,efac)), nodeLabel);
        formN = boolop::land(formN, nlbl);
      }
      
      collectAssumptions collector(assumptions);
      dagVisit(collector, formN);
      
      z3_is_sat_assuming (mk<AND>(formP, formN), assumptions, trueAssumptions);
      errs () << "\t tot number of nodes " << assumptions.size() <<"\n";
      errs () << "\t tot number of nodes in inv" << trueAssumptions.size() <<"\n";
	

      size_t asmSize = trueAssumptions.size();

      for (int i = 0; i < 1; ++i){
	replaceAssumptionsFalse r(trueAssumptions, mk<TRUE>(efac), mk<FALSE>(efac));
	Expr cleanExpr = dagVisit(r, formN);
	 
        Stats::resume("ufo.simplify");	
	Simp simp(mk<TRUE>(efac), mk<FALSE>(efac), efac);
	formN = dagVisit(simp, cleanExpr);
        Stats::stop("ufo.simplify");	

        assumptions.clear();
	foreach (Expr n, trueAssumptions){
	  assumptions.push_back(n);
	}
	trueAssumptions.clear();

        z3_is_sat_assuming (mk<AND>(formP, formN), 
			    assumptions, trueAssumptions);

        errs () << "\t tot number of nodes " << assumptions.size() <<"\n";
        errs () << "\t tot number of nodes in inv" << trueAssumptions.size() <<"\n";
	
	if (asmSize < trueAssumptions.size() + 5) break;
	asmSize = trueAssumptions.size();
      }
      
      replaceAssumptions r(trueAssumptions, mk<TRUE>(efac), mk<FALSE>(efac));
      Expr cleanExpr = mk<NEG>(dagVisit(r, formN));
      
      errs () << "\t tot number of nodes " << assumptions.size() <<"\n";
      errs () << "\t tot number of nodes in inv" << trueAssumptions.size() <<"\n";
/*      
      assumptions.clear();
      if (trueAssumptions.size() != 1){
	Expr formN = mk<TRUE>(efac);
	foreach (Node* n, nodes){
          if (trueAssumptions.count(mk<ASM>(mkTerm(n,efac))) <= 0) continue;
	  errs() << "Adding neg\n";
	  Expr assum = mk<ASM>(mkTerm(n,efac));
	  assumptions.push_back(assum);
	  formN = boolop::land(formN, mk<IMPL>(assum,mk<NEG>(node2lbl[n])));
	}

	trueAssumptions.clear();
	z3_is_sat_assuming(mk<AND>(formP, formN), assumptions, trueAssumptions);
	
	errs () << "\t tot number of nodes " << assumptions.size() <<"\n";
	errs () << "\t tot number of nodes in inv" << trueAssumptions.size() <<"\n";
   
      }

      foreach (Node* n, nodes){
        if (trueAssumptions.count(mk<ASM>(mkTerm(n,efac))) > 0){
	  Expr lbl;
	  if (n->getLabel())
	    lbl = n->getLabel();
	  else 
	    lbl = adom.gamma(n->getLoc(), n->getState());
	  
	  errs () << "Adding node to invariant\n";
	  cp2inv[cp] = boolop::lor(cp2inv[cp], lbl);
	}
      }*/
      cp2inv[cp] = z3_lite_simplify(cleanExpr);
    }
    
    void simplifyCutPoint(CutPointPtr cp){
      errs() << "Computing inv for " << cp->getNameStr() << "\n";
      std::vector<Node*>& nodes = cp2nodes[cp];
      cp2inv[cp] = mk<FALSE>(efac);
      ExprZ3 z3(efac);
      z3.elim_and(true);
      z3.restart();

      ExprZ3 simp(efac);


      AbstractState a = adom.bot(cp);
      foreach (Node* n, nodes){
	if (!n->getState()) continue;
        a = adom.join(cp, a, n->getState());
      }

      Expr ae = adom.gamma(cp, a);
      z3.assertExpr(mk<NEG>(ae));

      cp2inv[cp] = ae;
      rev_foreach (Node* n, nodes){
	z3.push();
	// -- only consider nodes with labels
	if (n->getState()) continue;
	Expr ne = n->getLabel ();

	z3.assertExpr(ne);
	
	if (z3.solve()){
	  z3.pop();
	  cp2inv[cp] = boolop::lor(cp2inv[cp], ne);
	  z3.assertExpr(mk<NEG>(ne));
	}else{
	  errs () << "Disregarding label\n";
	  z3.pop();
	}
      
      }
      //cp2inv[cp] = z3_simplify(cp2inv[cp]);
    }
    
    public: 
    std::map<CutPointPtr, Expr> cp2inv;
    
    void reset() { 
      std::map<CutPointPtr, ExprZ3*>::iterator it;
      
      for (it = cp2solver.begin(); it != cp2solver.end(); ++it){
        delete (*it).second;
      }
	
      cp2inv.clear(); 
      cp2solver.clear(); 
      cp2nodes.clear();
    }
    
    void simplifyInv(){
      errs () << "Simplifying INV\n";
      cp2inv.clear();
      
      Stats::resume("Invariant.simplifyInv");
      std::map<CutPointPtr, std::vector<Node*> >::iterator it;
      for (it = cp2nodes.begin(); it != cp2nodes.end(); ++it){
        simplifyCutPoint2 ((*it).first);
      }
      errs () << "Done Simplifying INV\n";
      Stats::stop("Invariant.simplifyInv");
    }
    
    /** returns true iff e is added
      */
    bool add(Node* n, Expr e){
      CutPointPtr cp = n->getLoc();

      Stats::resume("Invariant.add");
      if (cp2solver.count(cp) == 0){
        cp2solver[cp] = new ExprZ3(efac);
        cp2solver[cp]->elim_and(true);
	cp2solver[cp]->restart();
	cp2solver[cp]->assertExpr(mk<NEG>(e));
       
	cp2inv[cp] = e;
        cp2nodes[cp].push_back(n);
        Stats::stop("Invariant.add");
	return true;
      }

      ExprZ3* z3 = cp2solver[cp];
      assert(z3);

      z3->push();

      z3->assertExpr(e);

      if (z3->solve()){
        
	//SAT: not subsumed
	z3->pop();
	z3->assertExpr(mk<NEG>(e));

	cp2inv[cp] = boolop::lor(cp2inv[cp], e);
        cp2nodes[cp].push_back(n);
        Stats::stop("Invariant.add");
	return true;
      }else{
	
	//UNSAT: subsumed
        z3->pop();
        Stats::stop("Invariant.add");
	return false;
      
      }
    }

    Expr get(CutPointPtr cp){
      assert(cp2inv.count(cp) > 0);
      return cp2inv[cp];
    }
    
    Invariant(ExprFactory& efac_, AbstractStateDomain& dom): efac(efac_), adom(dom) {}
  };
}

#endif
