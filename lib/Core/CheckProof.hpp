#ifndef _UFO_CHECK_PROOF__HPP_
#define _UFO_CHECK_PROOF__HPP_

#include "ufo/ufo.hpp"
#include "ufo/Arg.hpp"
#include "ufo/Expr.hpp"
#include "ufo/Asd/AbstractStateDomain.hpp"

namespace ufo
{  
  using namespace boost;
  using namespace llvm;
  using namespace expr;

  
  class CheckProof 
  {
    ExprFactory &efac;
    AbstractStateDomain &adom;
    LBE &lbe;
    ARG &aArg;

    Node *root;

    /** node at which proof failed */
    Node *failedNode;
    /** Destination of a failed edge */
    Node *failedDst;
  
    
  
  public:
    CheckProof (ExprFactory &fac, AbstractStateDomain &dom, LBE &cpG, ARG &arg):
      efac (fac), adom (dom), lbe (cpG), aArg (arg)
    { 
      root = aArg.getRoot ();
      failedNode = NULL;
      failedDst = NULL;
    }
  
    /** Returns a node at which the proof failed (or NULL) */
    Node *getFailedNode () { return failedNode; }
    /** Returns a destination of a failed edge (or NULL ) */
    Node *getFailedDst () { return failedDst; }
  
  
    /** Return true if the current labeling of the ARG is well-formed, 
     *  false if it is not, and maybe if SMT solver failed. */
    boost::tribool validate () 
    {
      NodeSet visited;
      return validate (root, visited);
    }

    boost::tribool checkInvariant(std::map<CutPointPtr, Expr>& cp2inv){
      errs () << "Checking invariant\n"; 
      
      std::map<CutPointPtr, Expr>::iterator it;
      
      for (it = cp2inv.begin(); it != cp2inv.end(); ++it){
        CutPointPtr cpPre = (*it).first;
	Expr pre = (*it).second;
	
	for (edgeIterator ei = cpPre->succBegin(); ei != cpPre->succEnd(); ++ei){
	  SEdgePtr edge = *ei;
	  CutPointPtr cpPost = edge->getDst();
	  errs () << "\tChecking edge " << cpPre->getNameStr() << " ==> " << cpPost->getNameStr() <<"\n";
	  if (cp2inv.count(cpPost) <= 0)  { errs () << "\tEdge unreachable\n"; continue; }
	  if (!checkLabels(pre, cpPre, cp2inv[cpPost], cpPost))
	      return false;  
	}
      }

      errs () << "Done Checking Invariant\n";

      return true;
    }
    
    boost::tribool checkLabels (Expr pre, CutPointPtr src, 
				Expr post, CutPointPtr dst)
    {
      SEdgePtr edge = *(src->findSEdge (*dst));

      Environment env (efac);
      Expr ant = SEdgeCondComp::computeEdgeCond (efac, env, pre, edge, lbe);

      boost::tribool res = !isSAT (mk<AND>(ant, mk<NEG> (env.eval (post))));

      if (res == false)
	{
	  errs () << "FAILED" << "\n"
		  << "pre: " << *boolop::pp (pre) << "\n\n"
		  << "post: " << *boolop::pp (post) << "\n\n"
		  << "ant (pre&&edge): " << *boolop::pp(ant) << "\n\n"
		  << "evPost: " << *boolop::pp (env.eval (post)) << "\n\n";
	  

	  // errs () << "SIMPLE:\n"
	  // 	  << "pre: " << * (simplify (pre)) << "\n\n"
	  // 	  << "post: " << * (simplify (post)) << "\n\n"
	  // 	  << "ant: " << * (simplify (ant)) << "\n\n";
	}
      return res;
      
    }
    
    boost::tribool checkLabels (Expr pre, Node* src, Expr post, Node* dst)
    {
      SEdgePtr edge = *(src->getLoc ()->findSEdge (*(dst->getLoc ())));

      Environment env (efac);
      Expr ant = SEdgeCondComp::computeEdgeCond (efac, env, pre, edge, lbe);

      boost::tribool res = !isSAT (mk<AND>(ant, mk<NEG> (env.eval (post))));

      if (res == false)
	{
	  errs () << "FAILED"
		  << (aArg.isFalseEdge (src, dst) ? " (FALSE_EDGE) " : " ")
		  << "\n"
		  << "pre: " << *boolop::pp (pre) << "\n\n"
		  << "post: " << *boolop::pp (post) << "\n\n"
		  << "ant (pre&&edge): " << *boolop::pp(ant) << "\n\n"
		  << "evPost: " << *boolop::pp (env.eval (post)) << "\n\n";

	  // errs () << "SIMPLE:\n"
	  // 	  << "pre: " << * (simplify (pre)) << "\n\n"
	  // 	  << "post: " << * (simplify (post)) << "\n\n"
	  // 	  << "ant: " << * (simplify (ant)) << "\n\n";
	}
      return res;
    }
    

    boost::tribool checkEdge (Node *src, Node *dst)
    {
      errs () << "Checking edge " 
	      << src->getId () << " -> " << dst->getId () << "\n";


      Expr pre = src->getLabel ();
      if (!pre) pre = adom.gamma (src->getLoc (), src->getState ());


      Expr post = dst->getLabel ();
      if (!post) post = adom.gamma (dst->getLoc (), dst->getState ());
      

      return checkLabels (pre, src, post, dst);
    }
  

  protected:
   
    boost::tribool validate (Node *n, NodeSet &visited)
    {
      // -- already seen
      if (visited.count (n) > 0) return true;
      // -- node from the wrong pass
      if (n->getVisit () != root->getVisit ()) return true;

      // -- remember for the future
      visited.insert (n);

      // -- if covered, check that properly covered
      if (n->isCovered ()) return checkCover (n);
    


      // -- no covered, check that all children a properly labeled
      foreach (Node *c, n->getChildren ())
	{
	  if (c->getVisit () != root->getVisit ()) continue;
	  boost::tribool res = checkEdge (n, c);
	  if (res != true) 
	    {
	      failedNode = n;
	      failedDst = c;
	      return res;
	    }
	  res = validate (c, visited);
	  if (res != true) return res;
	}

      return true;
    }

    boost::tribool checkCover (Node *n)
    {
      if (!n->isCovered ()) return true;
      Expr rhs = mk<FALSE> (efac);

      foreach (Node *c, n->getCoveredBy ())
	rhs = boolop::lor (c->getLabel () ? c->getLabel() : 
			   adom.gamma(c->getLoc(), c->getState()), rhs);
    
      Expr lhs = n->getLabel ();
      if (!lhs) lhs = adom.gamma (n->getLoc(), n->getState());

      return !isSAT (boolop::land (lhs, boolop::lneg (rhs)));
    }

    boost::tribool _isSAT (Expr e)
    {
      ExprMSat solver (efac);
      return solver.isSAT (e);
    }
    
    boost::tribool isSAT (Expr e)
    {
      ExprZ3 solver (efac, StringMap (), true);
      
      solver.assertExpr (e);
      boost::tribool res = solver.solve ();
      if (res == true) 
	{
	  errs () << "\n\n\n";
	  solver.debugPrintModel ();
	  errs () << "\n\n\n";
	}
      return res;
    }

    Expr simplify (Expr e) { return z3_simplify (e); }
    
  };
}


#endif
