/** 
 * Experimental code. Not maintained. Use at your own risk.
 */
   
#include "IncRefiner.hpp"

#include "ufo/Smt/ExprMSat.hpp"
#include "ufo/Smt/ExprZ3.hpp"
#include "ufo/Smt/ForallSimplify.hpp"


namespace ufo
{
  void IncRefiner::refine ()
  {
    assert (entryN != NULL);
    assert (exitN != NULL);
      
    errs() << "Refining: ";
    errs () << exitN->getLoc ()->getBB()->getNameStr() << "\n";
    
    std::vector<Expr> pathExpr;

    // -- compute nodes that can reach exitN
    std::set<Node*> reachable;      
    computeReachable (entryN, exitN, reachable);
                
    // -- compute topological order of the explored graph
    std::vector<Node*> orderedNodes;      
      
    getNodeTopologicalOrdering(exitN, entryN, reachable,
			       std::back_inserter (orderedNodes));      
    // -- remove exitN location from the list of nodes
    orderedNodes.pop_back();

    //foreach (Node* n, orderedNodes) errs () << name (n) << ", ";
		
    // -- edge to environment
    std::map<std::pair<int,int>, Environment> envMap;
    // -- edge to edge expression
    std::map<std::pair<int,int>, Expr> exprMap;
    // -- node to environment
    std::map<int,Environment> inScope; 
    // -- node to lbl
    std::map<Node*, Expr> node2lbl;
      
    std::map<Node*, Expr> resolvers;
    
    //double exprTimeA = getUsageTime ();      
    foreach (Node *curr, orderedNodes){
      computeEdgeExpr(curr, envMap, exprMap, inScope, reachable, node2lbl, false, resolvers);
    }
      
    // -- node to formula
    std::map<Node*, Expr> node2form;

    foreach (Node* curr, orderedNodes){
      pathExpr.push_back (computeNodeExpr(curr, exprMap, reachable, node2lbl));
      // -- store formula of each node
      node2form[curr] = pathExpr.back();
    }
    
    // -- set to interpolate
    interp.restart(true);
    interp.reset(true);

    // -- node to group
    std::map<Node*, int> node2group;
      
    boost::tribool res = false;
    int initGroup = -1;

    size_t pos = orderedNodes.size() - 1;
    std::set<Node*> asserted;
      
    // -- keep asserting exprs until all parents of exitN are in
    for (int i = orderedNodes.size() - 1; i >= 0; i--){
      Node* top = orderedNodes[i];
      Expr topExpr = node2form[top];
      node2group[top] = interp.assertAndGroup(topExpr);
      asserted.insert(top);

      // -- are all valid parents of exitN asserted?
      bool allExitNParents = true;
      foreach (Node* pe, exitN->getParents()){
	// XXX can we disregard false edges?
	if (arg.isFalseEdge (pe, exitN))
	  {
	    errs () << "disregarding edge\n";
	    continue;
	  }
          
	bool isIn = false;
          
	for (size_t j = i; j < orderedNodes.size(); j++){
	  if (pe == orderedNodes[j]){  
	    isIn = true;
	    continue;
	  }
	}

	if (!isIn) { allExitNParents = false; break; }
      }

      pos = i;
      // -- if all parents of exitN are in, break
      if (allExitNParents) { break; }
    }

    errs () << "POSITION = " << pos << "\n";
    // -- set backtrack point and assert initial nodes
    interp.push();
    std::vector<Node*> assertedV(asserted.begin(), asserted.end());
    initGroup = assertInitExpr(assertedV, 0, node2lbl, true);

    res = interp.solve();

    if (res == false){
      computeInterpolants2(orderedNodes, node2group, asserted, initGroup, inScope, node2form);
      return;
    }

    for (int i = ((int)pos) - 1; i >= 0; i--){
      // -- remove initial expression
      interp.pop ();
      errs () << "\nAsserting node " << i << "\n";
      Node* top = orderedNodes[i];
      Expr topExpr = node2form[top];

      node2group[top] = interp.assertAndGroup(topExpr);
      asserted.insert(top);
      interp.push();

      std::vector<Node*> assertedV2(asserted.begin(), asserted.end());
      initGroup = assertInitExpr(assertedV2, 0, node2lbl, true);

      res = interp.solve();
      if (res == false){
	computeInterpolants2(orderedNodes, node2group, asserted, initGroup, inScope, node2form);
	// compute interpolants
	return;
      }
    }

    errs () << "ExitN found (incremental refiner reporting)";
    return;
  }

  int IncRefiner::assertInitExpr(std::vector<Node*>& onodes, size_t pos,
				 std::map<Node*, Expr>& node2lbl, 
				 bool incRefine2)
  {
    // -- if all \mu_i have been asserted, assert true
    if (pos == 0 && !incRefine2)
      return interp.assertAndGroup(mk<TRUE>(efac));
 
    size_t ndisj = 0;
    // -- check if all parents of which node are there:
    Expr res = mk<FALSE>(efac);    
    for (size_t i = pos; i < onodes.size(); ++i){
      Node* n = onodes[i];
        
      // -- check if all parents of n are there
      bool allParents = true;
      foreach (Node* pe, n->getParents()){
	if (arg.isFalseEdge (pe, n)) continue;
          
	bool isIn = false;
          
	for (size_t j = pos; j < onodes.size(); j++){
	  if (pe == onodes[j]){  
	    isIn = true;
	    continue;
	  }
	}

	if (!isIn) { allParents = false; break; }
      }
      // -- root node
      if (n->getParents().empty()){
	res = mk<TRUE>(efac);
	ndisj++;
      }
      if (!allParents){
	ndisj++;
	  
	assert (node2lbl.count(n) > 0);
	//res = boolop::lor(res, boolop::land(mk<TRUE>(efac), mkTerm(n, efac)));
	res = boolop::lor(res, boolop::land(node2lbl[n], mkTerm(n, efac)));
	  
	  
	//XXX TEST
	//res = boolop::lor(res, boolop::land(mk<TRUE>(efac), mkTerm(n, efac)));
      }
    }
    errs () << "NUMBER OF INIT DISJ" << ndisj << "\n";
    return interp.assertAndGroup(res);
  }

  void IncRefiner::computeInterpolants2(std::vector<Node*>& orderedNodes, 
					std::map<Node*, int>& node2group,
					std::set<Node*>& asserted,
					int initGroup,
					std::map<int,Environment>& inScope,
					std::map<Node*, Expr>& node2cons)
  {

    // -- first compute groups
    int* groups = new int[orderedNodes.size()+1];
    groups[0] = initGroup;
    size_t gpos = 1;

    std::vector<Node*> nodes;
    for (size_t i = 0; i < orderedNodes.size(); ++i){
      Node* n = orderedNodes[i];
      if (asserted.count(n) > 0){
	groups[gpos] = node2group[n];
	nodes.push_back(n);
	gpos++;
      }
    }

    errs () << "MSAT interpolate start at incRefine2";
    std::vector<Expr> interps;      
    interp.interpolate(groups, gpos, std::back_inserter(interps));
    errs () << "MSAT interpolate DONE: " << interps.size() << "\n";
      
    errs () << "nodes size " << nodes.size() << "\n";
    errs () << "gpos " << nodes.size() << "\n";
    assert(interps.size() == nodes.size());

    for (size_t i = 0; i < nodes.size(); ++i){
      Node* curr = nodes[i];
      Expr cleanLabel = 
	z3_simplify (mkInScope(interps [i], inScope[curr->getId ()], curr));

      prevInterps[curr] = interps[i];
      Expr finalLabel = boolop::nnf(cleanLabel);
	
      labels[curr] = finalLabel;
      errs () << "\n\n\nInterpolant for: " << curr->getId() << "\n";
      errs () << "group " << groups[i] << "\n\n";
      //std::cout << interps[i] << flush;

      // -- check interpolants are inductive 
      //if (i < nodes.size() - 1)
      //assert (!interp.isSAT(mk<NEG>(mk<IMPL>(mk<AND>(interps[i],node2cons[curr]), interps[i+1]))));
      //else
      //assert (!interp.isSAT(mk<NEG>(mk<IMPL>(mk<AND>(interps[i],node2cons[curr]), mk<FALSE>(efac)))));
    }

    // -- set all other nodes to their current label
    for (size_t i = 0; i < orderedNodes.size(); ++i){
      if (labels.count(orderedNodes[i]) <= 0)
	labels[orderedNodes[i]] = orderedNodes[i]->getLabel();
    }

    // -- need to set all unlabeled 
    // -- nodes to false
      
    labels[exitN] = mk<FALSE>(efac);
    delete [] groups;
  }

  void IncRefiner2::refine()
  {
    assert (entryN != NULL);
    assert (exitN != NULL);
    errs() << "Refining: ";
    errs () << exitN->getLoc ()->getBB()->getNameStr() << "\n";
    
    std::vector<Expr> pathExpr;

    // -- compute nodes that can reach exitN
    std::set<Node*> reachable;      
    computeReachable (entryN, exitN, reachable);
                
    // -- compute topological order of the explored graph
    std::vector<Node*> orderedNodes;      
      
    getNodeTopologicalOrdering(exitN, entryN, reachable,
			       std::back_inserter (orderedNodes));
      
    //foreach (Node* n, orderedNodes) errs () << name (n) << ", ";
		
    // -- edge to environment
    std::map<std::pair<int,int>, Environment> envMap;
    // -- edge to edge expression
    std::map<std::pair<int,int>, Expr> exprMap;
    // -- node to environment
    std::map<int,Environment> inScope; 
    // -- node to formula
    std::map<Node*, Expr> node2form;
    // -- node to formula
    std::map<Node*, int> node2group;
    // -- node to lbl
    std::map<Node*, Expr> node2lbl;
      
    // -- remove exitN location from the list of nodes
    orderedNodes.pop_back();
      
    std::map<Node*, Expr> resolvers;
    
    //double exprTimeA = getUsageTime ();      
    foreach (Node *curr, orderedNodes){
      computeEdgeExpr(curr, envMap, exprMap, inScope, reachable, node2lbl, false, resolvers);
    }
      
    foreach (Node* curr, orderedNodes){
      pathExpr.push_back (computeNodeExpr(curr, exprMap, reachable, node2lbl));
      // -- store formula of each node
      node2form[curr] = pathExpr.back();
    }
    
    // -- set to interpolate
    interp.restart(true);
    interp.reset(true);
      
    int initGroup = -1;
      
    std::set<Node*> asserted;
    std::set<Node*> subGraph;
      
    int size = 0;


    errs () << "\n==pushing==\n";
    interp.push();

    while (true){

      // POP
      errs () << "\n==popping==\n";
      interp.pop();

      extendSubgraph(subGraph, reachable, exitN);
      int new_size = subGraph.size();
      errs () << "ONODES size: " << orderedNodes.size() << "\n";
      errs () << "subGraph size: " << subGraph.size() << "\n";
        
      if (new_size == size) break;
      size = new_size;

      foreach(Node* n, subGraph){
	if (asserted.count(n) <= 0){
	  errs () << "Asserting: " << n->getId() << "\n";
	  node2group[n] = interp.assertAndGroup(node2form[n]);
	  errs () << "Group " << node2group[n] << "\n";
	  asserted.insert(n);
	}
      }

      // PUSH
      errs () << "\n==pushing==\n";
      interp.push();

      //if (test && subGraph.size() != orderedNodes.size()) continue;
      std::vector<Node*> subGraphV(subGraph.begin(), subGraph.end());
      errs () <<"\nasserting init expr ";
      initGroup = assertInitExpr(subGraphV, 0, node2lbl, true);
      errs () << "\nGroup " << initGroup << "\n";

      errs () << "\nInremental refine: solving\n";
      bool res = interp.solve();
      errs () << "\nInremental refine: done solving\n";
	
      if (res == true) continue;
      else{
	errs () << "\nInremental refine2: interpolating\n";
	computeInterpolants2(orderedNodes, node2group, asserted, initGroup, inScope, node2form);
	errs () << "\nInremental refine2: done interpolating\n";
	break;
      }
    }

    // -- there are interpolants
    if (!labels.empty()){
      Expr conj = mk<FALSE>(efac);
      foreach (Node* n, orderedNodes){
	conj = boolop::land(conj, node2form[n]);
      }
      assert(!interp.isSAT(conj));
    }
    return;
  }
  
  void IncRefiner2::extendSubgraph (std::set<Node*>& subGraph, 
				    std::set<Node*>& reachable, Node* error)
  {
    if (subGraph.empty()){
      foreach (Node* ep, error->getParents()){
	if (!arg.isFalseEdge (ep, error)) subGraph.insert(ep);
      }
    }else{
      // -- otherwise
      foreach (Node* n, subGraph){
	foreach (Node* np, n->getParents()){
	  if (!arg.isFalseEdge (np, n)) subGraph.insert(np);
	}
      }
    }

    // -- fixpoint computation
    while(true){
      std::set<Node*> fixpoint;
	
      foreach (Node* n, subGraph){
	foreach (Node* nc, n->getChildren()){
	  if (reachable.count(nc) <= 0) continue;
	  if (nc == error) continue;
	  if (subGraph.count(nc) > 0 || fixpoint.count(nc) > 0)
	    continue;
	  if (!arg.isFalseEdge (n, nc)) fixpoint.insert(nc);
	}
      }
	
      subGraph.insert(fixpoint.begin(), fixpoint.end());
      if (fixpoint.empty()) break;
    }
  }
  
  void IncRefiner3::refine ()
  {
    assert (entryN != NULL);
    assert (exitN != NULL);
      
    errs() << "Refining: ";
    errs () << exitN->getLoc ()->getBB()->getNameStr() << "\n";
    
    //pruneArg();
    std::vector<Expr> pathExpr;

    // -- compute nodes that can reach exitN
    std::set<Node*> reachable;      
    computeReachable (entryN, exitN, reachable);
            
    // -- compute topological order of the explored graph
    std::vector<Node*> orderedNodes;      
      
    getNodeTopologicalOrdering(exitN, entryN, reachable,
			       std::back_inserter (orderedNodes));

    //foreach (Node* n, orderedNodes) errs () << name (n) << ", ";
		
    // -- edge to environment
    std::map<std::pair<int,int>, Environment> envMap;
    // -- edge to edge expression
    std::map<std::pair<int,int>, Expr> exprMap;
    // -- edge to edge expression
    std::map<Node*, Expr> nodeExpr;
    // -- node to group
    std::map<Node*, int> node2group;
    // -- node to environment
    std::map<int,Environment> inScope;
    // -- node to lbl
    std::map<Node*, Expr> node2lbl;
    node2lbl[exitN] = mk<TRUE>(efac);
      
    std::set<Node*> asserted;
    // -- remove exitN location from the list of nodes
    orderedNodes.pop_back();
    
    std::map<Node*, Expr> resolvers;

    //double exprTimeA = getUsageTime ();      
    foreach (Node *curr, orderedNodes)
      computeEdgeExpr(curr, envMap, exprMap, inScope, reachable, node2lbl, false, resolvers);

    foreach (Node* curr, orderedNodes){
      pathExpr.push_back (computeNodeExpr(curr, exprMap, reachable, node2lbl));
      nodeExpr[curr] = pathExpr.back();
      //if (prevNodeExpr.count(curr) > 0){
      //std::cout << "\n\nprev: " << prevNodeExpr[curr] << "\n";
      //std::cout << "\n\nnew: " << nodeExpr[curr] << "\n";
      //}
    }

    //double exprTimeB = getUsageTime ();

    //errs () << "\n Computed Expressions " 
    //  << (exprTimeB - exprTimeB) << "\n" ;      
    //exprTime += exprTimeB - exprTimeA;       
                
    // -- state interpolants
    std::vector<Expr> interps;
      

    int cutoff = -1;
      
    errs () << "\nsize of prevOrderedNodes: " << prevOrderedNodes.size();
    errs () << "\nsize of orderedNodes: " << orderedNodes.size();
    // -- iterate over ordered nodes until they differ from
    // -- previous nodes, that's the cutoff point
    for (size_t i = 0; i < prevOrderedNodes.size(); ++i){
      if (prevOrderedNodes.size() > orderedNodes.size()) break;
      if (prevOrderedNodes.size() == 0) break;
      Node* n = orderedNodes[i];
      if (n != prevOrderedNodes[i]){
	cutoff = i;
	break;
      }
      if (nodeExpr[n] != prevNodeExpr[n]){
	cutoff = i;
	break;
      }
    }

    // TEST
    errs () << "\n\nCUTOFF: " << cutoff << "\n";

    errs () << "\nprev: ";
    foreach (Node* n, prevOrderedNodes){
      errs () << n->getId() << ", ";
    }
      
    errs () << "\norder: ";
    foreach (Node* n,orderedNodes){
      errs () << n->getId() << ", ";
    }
    // TEST

      
    if (cutoff != -1 && cutoff == ((long)prevOrderedNodes.size()) - 1)
      cutoff = -1;

    interp.restart(true);

    int initGroup;

    for (int i = orderedNodes.size() - 1; i >= 0; --i){
      Node* n = orderedNodes[i];
      boost::tribool res = true;
      if (i >= cutoff){
	//if (1){  
	errs () << "Asserting out of cutoff " << n->getId() << "\n";
	node2group[n] = interp.assertAndGroup(nodeExpr[n]);
	asserted.insert(n);

	res = interp.solve();
      }else{ // i < cutoff
	errs () << "Asserting inside of cutoff " << n->getId() << "\n";
	errs () << "state formula of " << prevOrderedNodes[i+1]->getId() << "\n";
	assert(orderedNodes[i] == prevOrderedNodes[i]);
	  

	interp.push ();
	initGroup = interp.assertAndGroup(prevInterps[prevOrderedNodes[i+1]]);

	res = interp.solve();
	  
	if (res != false){
	  interp.pop ();
	  errs () << "Asserting actual " << n->getId() << "\n";
	  node2group[n] = interp.assertAndGroup(nodeExpr[n]);
	  asserted.insert(n);
	  if (i == 0) res = interp.solve();
	}
	  
      }

      assert (!boost::indeterminate(res));
      if (res == false){
	// -- there is no state formula, assert true
	if (asserted.size() == orderedNodes.size())
	  initGroup = interp.assertAndGroup(mk<TRUE>(efac));

	// -- compute interpolants
	//prevInterps.clear();
	computeInterpolants2(orderedNodes, node2group, asserted, initGroup, inScope, nodeExpr);

	// -- set unlabeled
	labels [exitN] = mk<FALSE>(efac);
	foreach (Node* n, orderedNodes){
	  if (labels.count(n) <= 0)
	    labels[n] = n->getLabel();
	}
	break;
      }else if (i == 0)
	break;

    }
      
    /////////////////////////TESTING//////////////////////////
    /*if (!nodeLabels.empty()){
      for(size_t i = 0; i<orderedNodes.size(); ++i){
      if (asserted.count(orderedNodes[i])>0){
      errs () << "Checking\n";
      Expr res = mk<TRUE>(efac);
      for (size_t j=0; j < i; ++j){
      res = boolop::land(nodeExpr[orderedNodes[j]], res);
      }
      assert(!interp.isSAT(mk<NEG>(mk<IMPL>(res, prevInterps[orderedNodes[i]]))));
      if (i == orderedNodes.size() - 1){
      assert(!interp.isSAT(mk<AND>(nodeExpr[orderedNodes[i]], prevInterps[orderedNodes[i]])));
      }else{
      assert(!interp.isSAT(mk<NEG>(mk<IMPL>(mk<AND>(prevInterps[orderedNodes[i]], nodeExpr[orderedNodes[i]]),
      prevInterps[orderedNodes[i+1]]))));
      }
      }
      }

      }	*/
    //////////////////////////////////////////////////////


    errs () << "Done updating labels " << "\n";
      
    // -- update prev* attributes
    prevOrderedNodes.clear();
    prevOrderedNodes = std::vector<Node*>(orderedNodes.begin(), 
					  orderedNodes.end());
      
    prevNodeExpr.clear();
    prevNodeExpr = std::map<Node*, Expr>(nodeExpr.begin(), nodeExpr.end());
      
    return ;
  }
  
}
