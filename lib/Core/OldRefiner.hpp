#ifndef __OLD_REFINER__HPP_
#define __OLD_REFINER__HPP_

#include "Refiner.hpp"

/**
   The old refiner interface. Will be removed when the new refiner is
   more stable.
 */

namespace ufo
{
  typedef std::map<IntPair,Environment> IntPairEnvMap;
  
  class OldRefiner : public Refiner
  {
  public:
    OldRefiner (ExprFactory &fac, 
		AbstractStateDomain &dom,
		LBE &cpG,
		DominatorTree &dt,
		ExprMSat &msat,
		ARG &a, bool doSimp, NodeExprMap& nodeLabels) :
      Refiner (fac, dom, cpG, dt, msat, a, doSimp, nodeLabels) {}
    
    virtual void refine ();

  protected:
  protected:
    /** Computes all nodes that can reach given node n */
    void computeCanReach(Node* n, NodeSet& out);
    /** Computes all nodes that are reachable from a given node n and
	stay within  the filter region */
    void computeReachFrom (Node *n, NodeSet &filter, NodeSet &out);
    /** Computes all nodes between entry and exit nodes */
    void computeReachable (Node *entry, Node *exit, NodeSet &out);
    
    void computeEdgeExpr (Node* node, std::map<IntPair,Environment> &envMap,
  			  std::map<IntPair, Expr> &exprMap,
  			  std::map<int, Environment> &inScope,
  			  std::set<Node*> &orderedNodes,
  			  std::map<Node*, Expr>& node2lbl,
			  bool addAssum,
			  std::map<Node*, Expr>& resolvers);
    void cleanMerge (Environment& env, Node *node);
    Expr mkInScope (Expr e, const Environment &env, Node* n);
    Expr computeNodeExpr (Node* node, 
			  std::map<IntPair, 
			  Expr>& exprMap,
			  std::set<Node*>& orderedNodes,
			  std::map<Node*,Expr>& node2lbl);
    

    /** Returns topological order of the nodes that are on any path
     * from entryN to dst, that pass through reachable region. 
     */
    template <typename OutputIterator> 
    void getNodeTopologicalOrdering (Node* dst, Node* entryN, 
  				     std::set<Node*> &reachable,
  				     OutputIterator out)
    {
      
      std::list<Node*> open;
      std::map<id_ty, size_t> numNodeVisits;
      open.push_back(entryN);

      while(!open.empty()){
  	Node *curr = open.back();
  	open.pop_back();
  	*(out++) = curr;

  	foreach (Node* child, curr->getChildren ())
	  {
	    if (reachable.count (child) <= 0) continue;
	    
	    id_ty child_id = child->getId ();
	        
	    if(numNodeVisits.count (child_id) <= 0)
	      {
		// -- compute number of parents, ignoring false edges
		size_t psize = 0;
		foreach (Node *p, child->getParents ())
		  if (reachable.count (p) > 0) psize++;

		// -- at least one parent
		assert (psize > 0);
						   
		//not visited yet, init
		numNodeVisits[child_id] = psize - 1;
	      }
	  
	    else //visited, update parent count
	      numNodeVisits[child_id]--;

	    if(numNodeVisits[child_id] == 0)
	      open.push_back(child);
	  }
      }
    }
  };

  class AsmRefiner : public OldRefiner
  {
  public:
    AsmRefiner (ExprFactory &fac, 
		AbstractStateDomain &dom,
		LBE &cpG,
		DominatorTree &dt,
		ExprMSat &msat,
		ARG &a, bool doSimp, NodeExprMap& nodeLabels) :
      OldRefiner (fac, dom, cpG, dt, msat, a, doSimp, nodeLabels) {}
   
       
    virtual void refine ();
  };

}


  

#endif
