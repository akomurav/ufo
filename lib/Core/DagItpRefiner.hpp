#ifndef __DAG_ITP_REFINER__HPP__
#define __DAG_ITP_REFINER__HPP__

/** A refiner based on DagInterpolate and ArgCond classes. 
 * 
 * Should replace the standard Refiner eventually.
 */

#include <list>

#include "ufo/ufo.hpp"

#include "ufo/Cpg/Lbe.hpp"
#include "ufo/Arg.hpp"

#include "ufo/EdgeComp.hpp"

#include "Refiner.hpp"
#include "ufo/Smt/ExprMSat.hpp"
#include "ufo/Smt/ExprZ3.hpp"

#include "ufo/ArgBgl.hpp"
#include "ufo/ArgCond.hpp"

#include "ufo/Smt/MUS.hpp"

#include "DagInterpolate.hpp"

#include "ufo/property_map.hpp"

#include "boost/graph/graph_traits.hpp"
#include "boost/graph/filtered_graph.hpp"
#include "boost/graph/topological_sort.hpp"
#include "boost/typeof/typeof.hpp"

#include "ufo/ufo_graph.hpp"
#include "ufo/ufo_iterators.hpp"

#include "UfoLinerPrinter.hpp"
  
namespace ufo
{
  class DagItpRefiner : public Refiner
  {
  private:

  public:
    typedef std::map<const BasicBlock*, Expr>  BbExprMap;

    DagItpRefiner (ExprFactory &fac, 
		   AbstractStateDomain &dom,
		   LBE &cpG,
		   DominatorTree &dt,
		   ExprMSat &msat,
		   ARG &a, bool doSimp, NodeExprMap& nodeLabels) :
      Refiner (fac, dom, cpG, dt, msat, a, doSimp, nodeLabels) {}
    
    void refine ()
    {
      assert (entryN != NULL);
      assert (exitN != NULL);

      /** Counterexamples are only computed once. There is no
       * refinement after that 
       */
      assert (argCex.empty ());
      assert (cfgCex.empty ());
      

      labels.clear ();      
      labels [entryN] = mk<TRUE> (efac);
      labels [exitN] = mk<FALSE> (efac);
      
      // errs () << "DagItpRefiner: refine path to "
      // 	      << exitN->getLoc ()->getBB()->getNameStr() << " ...\n";
      
      // errs () << "Initial vertices\n";
      // foreach (Node *v, vertices (arg))
      // 	errs () << "NID(" << v->getId () << ")\n";
      // errs () << "END Initial vertices\n";
      
      // -- compute the set of nodes that are between entryN and exitN
      NodeSet reachable;
      BOOST_AUTO(reachablePM, make_set_property_map (reachable));
      {
	BOOST_AUTO(nfeArg, 
		   make_filtered_graph (arg, NotFalseEdgePredicate<ARG> (arg)));
	slice (nfeArg, entryN, exitN, reachablePM);
      }
      
      
      // -- restrict the graph to nodes between entryN and exitN (but
      // -- keep false edges)
      // BOOST_AUTO(rArg, 
      // 		 make_filtered_graph 
      // 		 (arg, make_vertex_edge_predicate 
      // 		  (arg, reachablePM)));
      BOOST_AUTO(rArg, 
		 make_filtered_graph 
		 (arg, make_vertex_edge_predicate (arg, reachablePM), 
		  make_vertex_predicate (arg, reachablePM)));
      
      
      // errs () << "entry " << entryN->getId () 
      // 	      << "exit " << exitN->getId () << "\n";
      
      // -- compute topological order of the ARG
      NodeList topo;
      topological_sort (rArg, std::front_inserter (topo));

      // errs () << "topo-sort\n";
      // foreach (Node *n, topo) errs () << "NID(" << n->getId () << ")\n";

      // -- compute the arg condition (with assumptions)
      ArgCond< BOOST_TYPEOF(rArg) > aCondComp (efac, lbe, DT, true);
      std::map<Node*,Environment> envMap;
      std::map<NodePair,Expr> argCond;
      std::map<NodePair,Environment> edgEnvMap;
      BOOST_AUTO (envMapPM, make_assoc_property_map (envMap));
      BOOST_AUTO (argCondPM, make_assoc_property_map (argCond));
      BOOST_AUTO (edgEnvPM, make_assoc_property_map (edgEnvMap));
      aCondComp.argCond (rArg, topo, envMapPM, argCondPM, edgEnvPM);

      NodeExprMap labelMap;

      {
	ExprVector vc (distance (topo));
	encodeVC (efac, rArg, topo, argCondPM, vc.begin ());


	if (consRefine)
	  {
	    // -- add lemmas from nodeLabels
	    typedef boost::tuple<Node*, Expr&> VE;
	    foreach (VE ve, 
		     make_pair
		     (mk_zip_it (++begin (topo), ++begin (vc)),
		      mk_zip_it (--end (topo), --end (vc))))
	      {
		Node *v = ve.get<0> ();
		Expr vTerm = mkTerm (v, efac);
	    
		// -- evaluate the label in the environment
		Expr nodeLabel = envMap [v].eval (nodeLabels [v]);
		// -- add assumptions
		addAssumptions addV;
		nodeLabel = dagVisit (addV, boolop::nnf (nodeLabel));
		// -- add assumption literal for the whole label
		nodeLabel = boolop::limp (mk<ASM> (vTerm), nodeLabel);
		// -- condition node label on the node v
		nodeLabel = boolop::limp (vTerm, nodeLabel);
	    
		// -- store the label 
		labelMap[v] =  nodeLabel;

		// -- add the label to the VC
		Expr &e = ve.get<1> ();
		e = boolop::land (nodeLabel, e);

	      }
	  }	

	// -- compute MUS
	ExprSet usedAssumes;
	ExprZ3 z3 (efac, StringMap(), true);
	Stats::resume ("refiner.mus");
	tribool res = mus_basic (z3, 
				 mknary<AND> (mk<TRUE> (efac), 
					      vc.begin (), vc.end ()),
				 std::inserter (usedAssumes, 
						usedAssumes.begin ()), 
				 3);
	Stats::stop ("refiner.mus");
	
	// -- SAT
	if (res || res == indeterminate)
	  {
	    labels.clear ();
	    if (res) 
	      { 
		if (!(ufocl::TRACE_FILENAME).empty ())
		  { 
		    // -- stream to write cex to
		    raw_ostream *out = &errs ();

		    // -- attept to open trace file
		    string errorStr;
		    raw_fd_ostream fs (ufocl::TRACE_FILENAME.c_str (),
				       errorStr, 0);
		    if (!errorStr.empty ())
		      errs () << "Cannot open: " << errorStr << "\n";
		    else
		      out = &fs;
			
		    errs () << "Computing CEX\n";
		    Stats::resume ("refiner.cex");
		    computeCex (rArg, z3, edgEnvMap);
		    Stats::stop ("refiner.cex");
		    errs () << "Done with CEX\n";
		    printDbgCex (*out, edgEnvMap, z3);
		    out->flush ();
		  }
	      }
	    return;
	  }

	Stats::resume ("refiner.kill.assumptions");
	// -- simplify ArgCond by keeping only used assumptions
	typedef std::map<NodePair,Expr>::value_type KV;
	foreach (KV kv, argCond) 
	  {
	    KillAssumptions ka (usedAssumes);
	    argCond [kv.first] = replaceSimplify (kv.second, mk_fn_map (ka));
	  }

	if (consRefine)
	  {
	    typedef NodeExprMap::value_type VT;
	    foreach (VT kv, labelMap)
	      {
		KillAssumptions ka (usedAssumes);
		labelMap [kv.first] = replaceSimplify (kv.second, 
						       mk_fn_map (ka));
	      }
	  }
	Stats::stop ("refiner.kill.assumptions");
      }
      
      
      
			 
      Stats::resume ("refiner.dag.interpolate.total");
      Stats::resume ("refiner.dag.interpolate");
      // -- compute DAG Interpolant
      ExprMSat msat(efac);
      msat.restart (true);
      msat.reset (true);
      DagInterpolate< BOOST_TYPEOF(rArg), ExprMSat > dagItpComp (efac, msat);
      BOOST_AUTO(labelsPM, make_assoc_property_map (labels));
	  
      tribool res;
      if (consRefine)
	{
	  BOOST_AUTO (labelMapPM, make_assoc_property_map (labelMap));
	  res = dagItpComp.dagItp (rArg, topo, envMapPM, 
				   argCondPM, labelMapPM, labelsPM);
	}
      else
	{
	  static_property_map<Expr> smap(mk<TRUE> (efac));
	  res = dagItpComp.dagItp (rArg, topo, envMapPM, 
				   argCondPM, smap, labelsPM);
	}
      
	
      Stats::stop ("refiner.dag.interpolate");

      if (res)
	{
	  // typedef std::pair<Node*,Expr> KV;
	  // errs () << "New labels\n";
	  // foreach (KV kv, labels)
	  //   errs () << kv.first->getId () << " " << *kv.second << "\n";
	  
	  assert (labels.size () == topo.size ());
	  

	  Stats::resume ("refiner.nnf");	  
	  // -- put labels into NNF (required by other passes)
	  for (NodeExprMap::iterator it = labels.begin (), end = labels.end ();
	       it != end; ++it)
	    it->second = boolop::gather (boolop::nnf (it->second));
	  Stats::stop ("refiner.nnf");	  	  

	  if (consRefine)
	    {
	      typedef NodeExprMap::value_type KV;
	      foreach (KV kv, labels)
		{
		  if (isLeaf (kv.first, rArg) || 
		      isRoot (kv.first, rArg)) continue;
		  
		  NodeExprMap::const_iterator it = nodeLabels.find (kv.first);
		  assert (it != nodeLabels.end ());
		  
		  labels [kv.first] = boolop::land (kv.second, it->second);
		}
	    }  

	  assert (isOpX<TRUE> (labels [entryN]));
	  assert (isOpX<FALSE> (labels [exitN]));
	}
      else labels.clear ();

      Stats::stop ("refiner.dag.interpolate.total");
    }

  private:
    template <typename Graph>
    void computeCex (const Graph &rArg, ExprZ3 &z3, 
		     std::map<NodePair, Environment> &edgMap)
    {
      /** compute the ARG cex */
      doArgCex (rArg, entryN, exitN, z3, std::back_inserter (argCex));
      
      typedef boost::tuple<Node*,Node*> NT;
      forall (NT nt, 
	      make_pair 
	      (mk_zip_it (begin (argCex), ++begin (argCex)),
	       mk_zip_it (--end (argCex), end (argCex))))
	{
	  Node* src = nt.get<0> ();
	  Node* dst = nt.get<1> ();
	  NodePair edge = make_pair (src, dst);
	  doCfgCex (src->getLoc ()->getBB (),
		  dst->getLoc ()->getBB (),
		  edgMap [edge], 
		  z3, 
		  std::back_inserter (cfgCex));
	}
      cfgCex.push_back (argCex.back ()->getLoc ()->getBB ()); 
    } 

    void printDbgCex (raw_ostream &out, 
		      std::map<NodePair, Environment> &edgEnvMap, 
		      ExprZ3 &z3)
    {
      NodeList::const_iterator argIt = argCex.begin ();
      NodeList::const_iterator argEnd = argCex.end ();
      BbList::const_iterator cfgIt = cfgCex.begin ();
      //BbList::const_iterator cfgEnd = cfgCex.end ();

      do
	{
	  assert (argIt != argEnd);
	  assert (std::distance (argIt, argEnd) >= 2);
			
	  Node* src = *argIt;
	  ++argIt;
	  Node* dst = *argIt;
			
	  NodePair edg = make_pair (src, dst);
	  out << "arg edge: " 
	      << edg.first->getId () 
	      << " --> "
	      << edg.second->getId () << "\n";
			
	  assert (edgEnvMap.count (edg) > 0);
	  Environment &edgEnv = edgEnvMap [edg];
	  printLines (out, *cfgIt, z3, edgEnv);
			
	  const Node *n = dst;
	  do
	    {
	      ++cfgIt;
	      const BasicBlock *bb = *cfgIt;
	      if (bb != n->getLoc ()->getBB ())
		printLines (out, bb, z3, edgEnv);    
	    }
			
	  while (*cfgIt != n->getLoc ()->getBB ());
	}
      while (std::distance (argIt, argEnd) >= 2);
    } 
  };    
}




#endif
