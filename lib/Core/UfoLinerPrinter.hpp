#ifndef __UFO_LINER_PRINTER__HPP_
#define __UFO_LINER_PRINTER__HPP_
#include "ufo/ufo.hpp"

#include "llvm/Support/InstVisitor.h"
#include "llvm/Support/raw_ostream.h"

#include "ufo/EdgeComp.hpp"

namespace ufo
{

  inline std::string constAsString (const ConstantExpr *gep)
  {
    assert (gep != NULL);
    assert (gep->isGEPWithNoNotionalOverIndexing ());
    
    const GlobalVariable *gv = 
      dyn_cast<const GlobalVariable> (gep->getOperand (0));
    assert (gv != NULL);
    assert (gv->hasInitializer ());

    const ConstantArray *op = 
      dyn_cast<const ConstantArray> (gv->getInitializer ());
    assert (op != NULL);
    assert (op->isCString ());
    return op->getAsString ();
 }
  

  template <typename Smt>
  void printLines (raw_ostream &out, const BasicBlock *bb, 
		   Smt &smt, Environment &env)
  {  
    out << "BasicBlock: " << bb->getName () << "\n";
    for (BasicBlock::const_iterator it = bb->begin (),
	   end = bb->end (); it != end; ++it)
      {
	if (const CallInst* ci = dyn_cast<const CallInst> (&*it))
	  {
	    Function *f = ci->getCalledFunction ();
	    if (f == NULL) continue;
	    if (f->getName ().equals ("__UFO_liner"))
	      {
		const ConstantInt *line = 
		  dyn_cast<const ConstantInt> (ci->getArgOperand (0));
		assert (line != NULL);
		
		const ConstantExpr *file = 
		  dyn_cast<const ConstantExpr> (ci->getArgOperand (1));
		assert (file != NULL);
		
		const ConstantExpr *scope = 
		  dyn_cast<const ConstantExpr> (ci->getArgOperand (2));
		assert (scope != NULL);
		

		// -- use c_str() to hide the trailing 0 char
		out << constAsString (file).c_str () << ":" 
		    << line->getSExtValue () << ":"
		    << constAsString (scope).c_str () << "\n";
	      }
	    else if (f->getName ().equals ("__UFO_fdecl"))
	      {
		const ConstantExpr *gep = 
		  dyn_cast<const ConstantExpr> (ci->getArgOperand (0));
		out << "enter: " << constAsString (gep) << "\n";
	      }
	    
	    continue;
	  }
	
	const Instruction *inst = &*it;
	if (isa<BranchInst> (inst)) continue;
	if (!inst->getType ()->isIntegerTy ()) continue;
	
	const Value *val = inst;
	Expr valE = mkTerm (val, env.getExprFactory ());
		if (env.isBound (valE))
	  {
	    Expr valS = env.lookup (valE);
	    Expr value = smt.getModelValue (valS);
	    if (value != valS && !isOpX<NONDET> (value))
	      out << "\t" << *valE << ": " << *value << "\n";
	  }	
      }
  } 
}



#endif
