#ifndef __VAR_BOUND_HPP_
#define __VAR_BOUND_HPP_

#include <boost/timer.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/optional.hpp>

#include "ufo/Asd/Interval.hpp"
#include "Equivalence.hpp"
#include "COI.hpp"
#include "ufo/Smt/ExprZ3.hpp"

#include "Bound.hpp"

using namespace expr;
using namespace expr::op;
using namespace boost;

namespace ufo
{
  /**
     Term: hold basic info of terms
     A term is of the form: x(...+/-...)y
  */
  struct Term
  {
    Term () {}

    Term (Expr _n, ExprSet _d = ExprSet(), bool _s = false, 
	  bool _b = false, 
	  mpq_class _bd = negInf())
      : name(_n), dep(_d), isSynthetic(_s), increasing(false), 
	isbool(_b), isSolved(false), bound(_bd), updateCount(1) {}

    ~Term () {}

    Expr name;
    ExprSet dep; // Depend on other non-synthetic terms

    bool isSynthetic; // Term is synthetic
    bool increasing; // Term is increasing
    bool isbool; // Term is Boolean variable
    bool isSolved; // Term is solved
    mpq_class bound; // Current upper bound
    unsigned updateCount; // Number of times that bound has been increased 

    void setBound (mpq_class b)
    {
      increasing = ! (leq (b, bound));
      if (updateCount % ufocl::BOUND_WIDEN == 0)
        // Widen if number of updates has exceed the limit
        bound = posInf();
      else
        bound = max (b, bound);

      if (increasing) ++updateCount;      
    }
  }; 

  /**
     Bound class.
     Customizable structure for calculating bounds for terms
     appeared in Boolean formula of linear constraints.
  */
  class VarBound : public Bound
  {
  private:
    ExprFactory &efac; // Expression Factory
    ExprSet &varbase; // Variables to be bounded
    //ExprZ3 z3; // Z3 object
    ExprSet realVars;
    ExprSet boolVars;

    ExprSet EQP; // Set of equalities (edges)
    typedef std::map<Expr,Term> WorkList;
    WorkList W; // Worklist holding terms to be bounded
    VarBounds vbs; // Results to be returned
    Equivalence<Expr> equi; // Equivalence relations
    COICache<Expr, mpq_class> &cache; // Cone of influence cache

  public:
    VarBound (ExprFactory& fac, ExprSet &_varbase, 
	      COICache<Expr, mpq_class> &_c) 
      : efac(fac), varbase(_varbase), //z3(efac),
        realVars(filterVars(varbase,true)),
        boolVars(filterVars(varbase,false)),
        equi (Equivalence<Expr>(_varbase)),
        cache (_c)
    {
      //z3.setModel (true);
      //z3.restart(); 
    } 

    ~VarBound ();

    typedef std::pair<Expr, Term> TermPair;
    /**
       Internal class for updating guess values
    */
    class UpdateGuess
    {
    public:
      /**
       * Callback run
       * param[in] ExprZ3 z3
       * param[in] Expr t: term being bounded
       * param[in] WorkList w: list of terms remaining
       * Return: true to abort, false to continue
       */
      static bool run (ExprZ3 &z3, Expr t, WorkList &W)
      {
	bool abort = false;

	foreach (TermPair p, W)
        {
          if (p.second.isSolved)
            continue;

          setBound (p.first, z3.getModelValue(p.first), W);

          if (W[t].isSynthetic &&
              isPosInf (W[p.first].bound) &&
              W[t].dep.count (p.first) > 0)
            abort = true;
        }
	return abort;
      }

      /**
       * Set bound. 
       * Input: term, bound, WorkList
       * Output: N.A
       * Side Effect: bounds for term updated
       */
      static void setBound (Expr t, Expr b, WorkList& W)
      {
	assert (W.count(t) > 0);
	if (!isOpX<MPQ>(b))
	  // Set bound to posInf if model is undefined
	  W[t].setBound (posInf());
	else 
	  W[t].setBound (getTerm<mpq_class>(b));
      }
    };

    /**
     * Initialization
     * Input: formula
     * Output: N.A
     * Side Effect: formula closed, EQP collected
     */
    void init (Expr e)
    {
      // Close the formula
      e = closeFormula(e);
      // Collect EQP
      scanCons(e);
      // Initialize vbs
      foreach (Expr _v, varbase)
        vbs[_v] = Interval::bot();
    }

    /** 
     * Scan linear constraints appeared in formula
     * Relate vars appeared in the same constraint.
     */
    void scanCons (Expr e);

    /** 
     * lub Compute least upper bound
     * assume: no boolean variables.
     * param[in] formula phi,
     * param[in] term t
     * param[out] result,
     * param[in] starting value
     * return: true, aborted; flase, normal exit
     * Side Effect: bounds of terms updated
     */
    template <typename Callback>
    bool lub (Expr phi, Expr t, mpq_class &s,
	      mpq_class start = negInf())
    {
      errs() << "Lub\n";
      // abort if start is unbounded
      if (isPosInf(start)) return true;

      Stats::count ("varbound.esc");

      ExprZ3 z3 (phi->efac(), StringMap(), true);
      
      ExprVector EQPcopy; // Make a deep copy of EQP.
      copy (EQP.begin(), EQP.end(), back_inserter(EQPcopy));

      z3.push();
      z3.assertExpr(phi);

      if (!isNegInf(start))
	z3.assertExpr(mk<GT>(t, mkTerm(start, efac)));


      //errs() << "Term: " << *t << "\n";
      while (z3.solve())
      {
        // Number of points (intersections) visited in the path
        Stats::count ("varbound.point");
        // abort if necessary.
        if (Callback().run(z3, t, W)) return true;

        Expr start_expr = z3.getModelValue(t);
        if (!isOpX<MPQ>(start_expr))
        {
          s = posInf();
          return false;
        }
        start = getTerm<mpq_class>(start_expr);

        z3.assertExpr(mk<GT>(t, start_expr));

        z3.push();
        int inner_count = 0;
        while (z3.solve())
        {
          Stats::count ("varbound.inner");
          ++inner_count;
              
          z3.assertExpr(disEQP(EQPcopy));
          if (z3.solve())
          {
            // abort if necessary.
            if (Callback().run(z3, t, W)) return true;

            start_expr = z3.getModelValue(t);
            if (!isOpX<MPQ>(start_expr))
            {
              errs() << "Inner_pos: " << inner_count << "\n";
              s = posInf();
              return false;
            }
            start = getTerm<mpq_class>(start_expr);

            for (vector<Expr>::iterator edge = EQPcopy.begin(); 
                 edge != EQPcopy.end();) 
            {
              if (isOpX<TRUE>(z3.getModelValue(*edge)))
              {
                z3.assertExpr(*edge);
                edge = EQPcopy.erase(edge);
              }
              else
                ++edge;
            }
            z3.assertExpr(mk<GT>(t, start_expr));
          }
          else
          {
            errs() << "Inner_pos: " << inner_count << "\n";
            s = posInf();
            return false; // exit normally.
          }
        }
        errs() << "Inner_normal: " << inner_count << "\n";
        z3.pop();
      }
      z3.pop();
      // store result
      s = start;
      return false;
    }

    /** Calculate Disjunction of Equalities (edges) */
    Expr disEQP (ExprVector &EQP)
    {
      return mknary<OR>(mk<FALSE>(efac), EQP.begin(), EQP.end());
    }

    /** Reference to current bound of term t */
    mpq_class& bound (Expr t)
    {
      assert(W.count(t) > 0);
      return W[t].bound;
    }

    /** Mark term t as solved */
    void solved (Expr t)
    {
      assert (W.count(t) > 0);
      W[t].isSolved = true;
      remove (t, W);
    }

    /** Test term t is synthetic or not */
    bool isSynthetic (Expr t)
    {
      assert (W.count(t) > 0);
      return W[t].isSynthetic;
    }

    /** Set of terms depended on by t */
    ExprSet& dep (Expr t)
    {
      assert (W.count(t) > 0);
      return W[t].dep; 
    }

    /** Remove t from the work list */
    void remove (Expr t, WorkList &W)
    {
      assert (W.count(t) > 0);
      if (!isSynthetic (t))
      {
        if (!isOpX<UN_MINUS>(t) && varbase.count(t) > 0)
          vbs[t].second = bound (t);
        else if (varbase.count(t->left()) > 0)
          vbs[t->left()].first = - bound (t);
      }
      W.erase (t);
    }

    /** Add term t
        to the work list */
    void addTerm (Expr t, WorkList &W)
    {
      assert (!isBool(t));
      W[t] = Term (t);
    }

    void addTerm (Term t, WorkList &W)
    {
      W[t.name] = t;
    }

    /** Searching algorithm */
    void searchBound (Expr phi);

    /** BoundAll. To be called by Bound abstract domain. */
    void boundAll (Expr phi);

    /** Computing bound for Boolean variable */
    //void boundBool(Expr e, Expr var);

    /** Synthesis term */
    optional<Term> synthesis ();

    /** Pick a term from the work list */
    Expr pick (WorkList &W);

    /** Overried operator [] */
    Interval operator [] (Expr var)
    {
      assert (vbs.count (var) > 0);
      return vbs[var];
    }
  };
}
#endif
