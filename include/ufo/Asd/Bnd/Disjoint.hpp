#ifndef __DISJOINT_SETS_H_
#define __DISJOINT_SETS_H_

#include <functional>

namespace ufo 
{
  /**
    Basic Disjoint Sets implementation with path compression.
    Operations:
    - make_set
    - union_set
    - find_parent
    - count_elements
    - count_sets
   */
  template <typename T, typename EqualTo = std::equal_to<T> >
  class DisjointSets
  {
    private:
      std::map<T, int> size; // size (rank) of the subtree.
      std::map<T, T> parent; // parent node.
    public:
      DisjointSets () {}

      DisjointSets (std::set<T> vars) 
      {
        foreach (T var, vars)
          make_set(var);
      }

      ~DisjointSets () {}

      // - return the number of elements
      virtual int count_elements () { return parent.size(); }

      // - return the number of disjoint sets 
      // Not considering efficiency. Just for debugging.
      typedef std::pair<T, T> TPair;
      virtual int count_sets ()
      {
        std::set<T> set;
        foreach (TPair p, parent)
        {
          set.insert(find_parent(p.first));
        }
        return set.size();
      }

      // - Make a set containing a single element
      virtual void make_set (T x)
      {
        parent[x] = x;
        size[x] = 0;
      }

      // - Find with path compression.
      virtual T find_parent (T x)
      {
        // Make a set if x is not found.
        if (parent.count(x) == 0)
          make_set(x);

        // Path compression.
        if (!(EqualTo() (x, parent[x])))
          parent[x] = find_parent(parent[x]);

        return parent[x];
      }

      // - Get the set containing elements related to x
      virtual std::set<T> get_set (T x)
      {
        std::set<T> set;
        foreach (TPair p, parent)
        {
          if (EqualTo() (p.second, parent[x]))
            set.insert(p.first);
        } 
        return set;
      }

      virtual void union_set (T x, T y)
      {
        x = find_parent(x);
        y = find_parent(y);

        if (EqualTo() (x, y))
          return;
        // Add smaller subtree to bigger one.
        if (size[x] < size[y])
          parent[x] = y;
        else
        {
          // Otherwise increse tree depth by one.
          if (size[x] == size[y])
            size[x]++;
          parent[y]=x;
        }
      }
  };

}
#endif

